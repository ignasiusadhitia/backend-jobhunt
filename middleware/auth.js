import jwt from 'jsonwebtoken'
import User from '../model/User.js';
export const getToken = (req)=>{
    let token = req.headers.authorization ? req.headers.authorization.replace("Bearer ","") : null;
    return token && token.length ? token : null
}
export const decodeToken = async (req,res,next)=>{
    try {
        let token = getToken(req)
        if (!token) {
            return res.send({message:"authentication failed"})
        }
        var verifyToken = jwt.verify(token, 'secret');
        const user = await User.findById(verifyToken.data).exec();
        if (user) {
            return next()  
        }
        return res.send({message:"authentication failed"})
    } catch (error) {
        return res.send({message:error.message})
    }
}
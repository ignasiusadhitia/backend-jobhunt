import express from "express";
import bodyParser from "body-parser";
import mongoose from "mongoose";
import authRouter from './router/auth.js'
import privateRouter from './router/private.js'
import {decodeToken} from './middleware/auth.js'
const app = express();
const port = 8000;

mongoose.connect('mongodb://127.0.0.1:27017/jobhunt');
const db = mongoose.connection
db.on('error',(error)=>console.log(error))
db.once('open',()=>console.log("connection success"))

app.use(bodyParser.json())

app.use('/',authRouter);

app.use((req,res,next)=>{
    decodeToken(req,res,next)
})
app.use('/',privateRouter)
app.listen(port,()=>{
    console.log("server running in port : "+port)
})
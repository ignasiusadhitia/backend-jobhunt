import express from 'express'
import { profile } from '../controller/user.js';
import { myAplication } from '../controller/application.js';
const router = express.Router();

router.post('/profile',profile)
router.post('/my-application',myAplication)

export default router;
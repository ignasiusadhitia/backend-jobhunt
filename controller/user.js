import User from "../model/User.js"
import bcrypt from 'bcrypt'
import jwt from "jsonwebtoken"
import { getToken } from "../middleware/auth.js"
export const profile = async(req,res)=>{
    
    let token = getToken(req)
    var verifyToken = jwt.verify(token, 'secret');
    const user = await User.findById(verifyToken.data).exec();
    return res.send({message:"success",data:user})
}
export const signin = async(req,res)=>{
    const {email,password} = req.body;
    const user = await User.findOne({email:email}).exec();
    if (user) {
        console.log("user find",JSON.stringify(user))
        const compare = bcrypt.compare(password, user.password).then(function(result) {
            return result
        });
        if (compare) {
            const token = jwt.sign({
                exp: Math.floor(Date.now() / 1000) + (60 * 60),
                data: user.id
              }, 'secret');
            return res.send({message:"signin success",token:token})
        }
    }
    return res.send({message:"email or password not found"})
}
export const register = async(req,res)=>{
    const {name,email,password} = req.body;
    const user = await User.create({name,email,password})
    await user.save()
    res.send({message:"Register Success"})
}